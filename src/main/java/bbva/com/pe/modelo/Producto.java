package bbva.com.pe.modelo;

public class Producto {

    long codigo;
    String tipoProducto;
    String descripcion;

    public Producto(long codigo, String tipoProducto, String descripcion) {
        this.codigo = codigo;
        this.tipoProducto = tipoProducto;
        this.descripcion = descripcion;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
