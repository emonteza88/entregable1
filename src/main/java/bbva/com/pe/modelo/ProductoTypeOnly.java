package bbva.com.pe.modelo;

public class ProductoTypeOnly {

    long codigo;
    String tipoProducto;

    public ProductoTypeOnly() {
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

}
