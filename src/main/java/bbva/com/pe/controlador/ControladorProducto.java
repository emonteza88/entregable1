package bbva.com.pe.controlador;

import bbva.com.pe.config.RutasApi;
import bbva.com.pe.modelo.Producto;
import bbva.com.pe.modelo.ProductoTypeOnly;
import bbva.com.pe.servicio.ServicioProducto;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(RutasApi.BASE)
public class ControladorProducto {

    @Autowired
    private ServicioProducto servicioProducto;

    // GET obtener todos los productos
    @GetMapping(RutasApi.PRODUCTOS)
    public List<Producto> listarProductos() {
        return servicioProducto.obtenerProductos();
    }

    // GET instancia por id
    @GetMapping(RutasApi.PRODUCTOS_ID)
    public ResponseEntity obtenerProductoPorId(@PathVariable long codigo) {
        Producto p = servicioProducto.obtenerProductoPorId(codigo);
        if (p == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(p);
    }

    @PostMapping(RutasApi.PRODUCTOS_ADD)
    public ResponseEntity<String> agregarProducto(@RequestBody Producto producto) {
        boolean response = servicioProducto.addProducto(producto);
        if(response) {
            return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Ocurrió un error al registrar el producto.", HttpStatus.NOT_FOUND);
        }
    }

    // PUT
    @PutMapping(RutasApi.PRODUCTOS_UPDATE)
    public ResponseEntity actualizarProducto(@PathVariable int codigo,
                                         @RequestBody Producto productoUpdate) {
        Producto pr = servicioProducto.obtenerProductoPorId(codigo);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        boolean response = servicioProducto.updateProducto(codigo, productoUpdate);
        if (response) {
            return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Ocurrió un error al actualizar el producto.", HttpStatus.NOT_FOUND);
        }
    }

    // DELETE
    @DeleteMapping(RutasApi.PRODUCTOS_UPDATE)
    public ResponseEntity eliminarProducto(@PathVariable int codigo) {
        Producto pr = servicioProducto.obtenerProductoPorId(codigo);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        boolean response = servicioProducto.deleteProducto(codigo);
        if (response) {
            return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Ocurrió un error al eliminar el producto.", HttpStatus.NOT_FOUND);
        }
    }

    // PATCH
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchTipoProducto(@RequestBody ProductoTypeOnly productoTypeOnly,
                                              @PathVariable int codigo){
        Producto pr = servicioProducto.obtenerProductoPorId(codigo);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setTipoProducto(productoTypeOnly.getTipoProducto());
        boolean response = servicioProducto.updateProducto(codigo, pr);
        if (response) {
            return new ResponseEntity<>("Producto actualizó el tipo de producto correctamente.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Ocurrió un error al realizar el patch del producto.", HttpStatus.NOT_FOUND);
        }
    }



}
