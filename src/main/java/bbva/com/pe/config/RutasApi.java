package bbva.com.pe.config;

public class RutasApi {

    public static final String BASE                   = "/productos/v1";
    public static final String PRODUCTOS              = "/productos";
    public static final String PRODUCTOS_ID           = "/productos/{codigo}";
    public static final String PRODUCTOS_ADD          = "/productos/addProducto";
    public static final String PRODUCTOS_UPDATE       = "/productos/update/{codigo}";

}
