package bbva.com.pe.servicio.imp;

import bbva.com.pe.modelo.Producto;
import bbva.com.pe.servicio.ServicioProducto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    private List<Producto> dataList = new ArrayList<Producto>();

    public ServicioProductoImpl() {
      /*  dataList.add(new Producto(1, "LOANS", "PRESTAMO AL TOQUE"));
        dataList.add(new Producto(2, "BILL PAYMENT", "PAGOS DE SERVICIOS"));
        dataList.add(new Producto(3, "CARDS", "TARJETAS"));
        dataList.add(new Producto(4, "ACCOUNTS", "CUENTAS PROPIAS"));
      */
    }

    private final AtomicLong secuenciaIds = new AtomicLong(0L);

    @Override
    public List<Producto> obtenerProductos() {
        return dataList;
    }

    @Override
    public Producto obtenerProductoPorId(long codigo) {
        if(getIndex(codigo)>=0) {
            return dataList.get(getIndex(codigo));
        }
        return null;
    }

    @Override
    public boolean addProducto(Producto producto) {
        int tam = this.dataList.size();
        final long id = secuenciaIds.incrementAndGet();
        producto.setCodigo(id);
        this.dataList.add(producto);
        if (tam < this.dataList.size()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updateProducto(int codigo, Producto producto) throws IndexOutOfBoundsException {
        dataList.set(codigo,producto);
        return true;
    }

    @Override
    public boolean deleteProducto(int codigo)  throws IndexOutOfBoundsException {
        dataList.remove(codigo);
        return true;
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getCodigo() == index){
                return(i); }
            i++;
        }
        return -1;
    }
}
