package bbva.com.pe.servicio;

import bbva.com.pe.modelo.Producto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ServicioProducto {

    public List<Producto> obtenerProductos();

    public Producto obtenerProductoPorId(long codigo);

    public boolean addProducto(Producto producto);

    public boolean updateProducto(int codigo, Producto producto);

    public boolean deleteProducto(int codigo);

}
